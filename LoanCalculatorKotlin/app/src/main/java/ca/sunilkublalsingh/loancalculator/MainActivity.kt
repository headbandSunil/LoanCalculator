package ca.sunilkublalsingh.loancalculator

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.EditText
import android.widget.TableLayout
import android.widget.TextView
import androidx.core.view.isInvisible

class MainActivity : AppCompatActivity() {
    private lateinit var etLoanAmount: EditText
    private lateinit var etNumberYears: EditText
    private lateinit var etYearlyInterest: EditText

    private lateinit var loanResTxt: TextView
    private lateinit var numYearsResTxt: TextView
    private lateinit var yearlyRateResTxt: TextView

    private lateinit var monthlyPaymentTxt: TextView
    private lateinit var totalPaymentTxt: TextView
    private lateinit var totalInterestTxt: TextView

    private lateinit var results : TableLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        etLoanAmount = findViewById(R.id.loanAmount)
        etNumberYears = findViewById(R.id.numYears)
        etYearlyInterest = findViewById(R.id.yearlyIntRate)

        loanResTxt = findViewById(R.id.loanAmountRes)
        numYearsResTxt = findViewById(R.id.numYearsRes)
        yearlyRateResTxt = findViewById(R.id.yearlyRateRes)

        monthlyPaymentTxt = findViewById(R.id.monthlyPayment)
        totalPaymentTxt = findViewById(R.id.totalPayment)
        totalInterestTxt = findViewById(R.id.totalInterest)

        results = findViewById(R.id.results)
    }

    fun calculate(v : View) {
        if(areFieldsGiven()) {
            // Get the fields to create LoanCalculator
            val loanAmount = etLoanAmount.text.toString().toDouble()
            val numYears = etNumberYears.text.toString().toInt()
            val yearlyInterest = etYearlyInterest.text.toString().toDouble()

            val calculator = LoanCalculator(loanAmount, numYears, yearlyInterest)

            val monthlyPayment = calculator.monthlyPayment
            val totalPayment = calculator.totalCostOfLoan
            val totalInterest = calculator.totalInterest

            displayValues(monthlyPayment.toString(), totalPayment.toString(), totalInterest.toString())
        }
    }

    fun clear(v : View) {
        etLoanAmount.setText("")
        etNumberYears.setText("")
        etYearlyInterest.setText("")

        results.visibility = View.INVISIBLE
    }

    private fun areFieldsGiven(): Boolean {
        if(etLoanAmount.text.isEmpty() || etNumberYears.text.isEmpty() || etYearlyInterest.text.isEmpty()) {
            return false
        }
        return true
    }

    private fun displayValues(
        monthlyPayment: String,
        totalPayment: String,
        totalInterest: String
    ) {
        loanResTxt.text = etLoanAmount.text.toString()
        numYearsResTxt.text = etNumberYears.text.toString()
        yearlyRateResTxt.text = etYearlyInterest.text.toString()

        monthlyPaymentTxt.text = monthlyPayment
        totalPaymentTxt.text = totalPayment
        totalInterestTxt.text = totalInterest

        results.visibility = View.VISIBLE
    }
}